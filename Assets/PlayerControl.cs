﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public KeyCode upButton = KeyCode.W;        // Tombol untuk menggerakkan ke atas

    public KeyCode downButton = KeyCode.S;      // Tombol untuk menggerakkan ke bawah

    public float speed = 10.0f;                 // Kecepatan gerak

    public float yBoundary = 9.0f;              // Batas atas dan bawah game scene (Batas Bawah menggunakan minus (-))

    private Rigidbody2D rigidBody2D;            // Rigidbody 2D raket ini

    private int score;                          // skor pemain

    private ContactPoint2D lastContactPoint;

    

    // Start is called before the first frame update
    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        // ===================================== Menggerakkan raket =====================================================================//

        Vector2 velocity = rigidBody2D.velocity;  // get kecepatan raket sekarang

        if (Input.GetKey(upButton)){
            velocity.y = speed;                     // Jika pemain menekan tombol ke atas, beri kecepatan positif ke komponen y (keatas)
        }else if (Input.GetKey(downButton)){
            velocity.y = -speed;                    // Jika pemain menekan tombol ke bawah, beri kecepatan negatif ke komponen y (kebawah)
        } else {
            velocity.y = 0;                         // Jika pemain tidak menekan tombol apa apa kecepatannya 0
        }

        rigidBody2D.velocity = velocity;            // Masukkan kembali kecepatannya ke rigidBody2D.

        // ==================================== Membatasi pergerakan raket ===============================================================//

        Vector3 position = transform.position;      // Dapatkan posisi raket sekarang.

        if (position.y > yBoundary){
            position.y = yBoundary;                 // Jika posisi raket melewati batas atas (yBoundary), kembalikan ke batas atas tersebut.
        } else if (position.y < -yBoundary){
            position.y = -yBoundary;                // Jika posisi raket melewati batas bawah (-yBoundary), kembalikan ke batas bawah tersebut.
        }

        transform.position = position;              // Masukkan kembali posisinya ke transform.
    }

    //===================================================== manajemen skor ===============================================================//
    public void incrementScore()
    {
        score++;
    }

    public void resetScore()
    {
        score = 0;
    }

    public int Score
    {
        get { return score; }
    }

    //===================================================== Debug =====================================================================//

    public ContactPoint2D LastContactPoint
    {
        get { return lastContactPoint; }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.Equals("Ball"))
        {
            lastContactPoint = collision.GetContact(0);
        }
    }

    
}
