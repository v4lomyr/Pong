﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour
{
    private Rigidbody2D rigidBody2D;    // Rigidbody 2D bola

    public float xInitialForce;         // Besarnya gaya awal yang di berikan untuk mendorong bola (sumbu x)
    public float yInitialForce;         // Besarnya gaya awal yang di berikan untuk mendorong bola (sumbu y)

    private Vector2 trajectoryOrigin;
    // Start is called before the first frame update
    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();

        trajectoryOrigin = transform.position;

        restartGame();                              // mulai game
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void resetBall()
    {
        transform.position = Vector2.zero;     // Reset posisi menjadi (0,0)
        rigidBody2D.velocity = Vector2.zero;   // Reset kecepatan menjadi (0,0)
    }

    void pushBall()
    {
        float randomDirection = Random.Range(0, 2);                                  // Tentukan nilai acak antara 0 (inklusif) dan 2 (eksklusif)

        if (randomDirection < 1.0f)
        {
            rigidBody2D.AddForce(new Vector2(-xInitialForce, yInitialForce));  // Jika nilainya di bawah 1, bola bergerak ke kiri.
        } 
        else
        {
            rigidBody2D.AddForce(new Vector2(xInitialForce, yInitialForce));   // Jika tidak, bola bergerak ke kanan.
        }
    }

    void restartGame()
    {
        resetBall();            // Kembalikan bola ke posisi semula
        Invoke("pushBall", 2);  // Setelah 2 detik, berikan gaya ke bola
    }

    private void OnCollisionExit(Collision collision)
    {
        trajectoryOrigin = transform.position;
    }

    public Vector2 TrajectoryOrigin
    {
        get { return trajectoryOrigin; }
    }
}
