﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // player 1
    public PlayerControl player1; // skrip P1
    private Rigidbody2D player1Rigidbody;

    // player 2
    public PlayerControl player2;
    private Rigidbody2D player2Rigidbody;

    // Ball
    public BallControl ball;
    private Rigidbody2D ballRigidbody;
    private CircleCollider2D ballCollider;

    // skor maximal
    public int maxScore;

    // Debug window
    private bool isDebugWindowShown = false;

    public Trajectory trajectory;

    // Start is called before the first frame update
    void Start()
    {
        player1Rigidbody = player1.GetComponent<Rigidbody2D>();
        player2Rigidbody = player2.GetComponent<Rigidbody2D>();
        ballRigidbody = ball.GetComponent<Rigidbody2D>();
        ballCollider = ball.GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnGUI()
    {
        // menampilkan skor pemain di atas ke 2 pemain
        GUI.Label(new Rect((Screen.width / 2) - 150 - 12, 20, 100, 100), "" + player1.Score);
        GUI.Label(new Rect((Screen.width / 2) + 150 + 12, 20, 100, 100), "" + player2.Score);

        if (GUI.Button(new Rect((Screen.width / 2) - 60, 35, 120, 53), "RESTART")) 
        {
            // ketika tombol restart di tekan
            player1.resetScore();
            player2.resetScore();

            // restart game
            ball.SendMessage("restartGame", 0.5f, SendMessageOptions.RequireReceiver);
        }

        if (player1.Score == maxScore)
        {
            // menampilkan teks player 1 win
            GUI.Label(new Rect((Screen.width / 2) - 150, (Screen.height / 2) - 15, 2000, 1000), "Player 1 Win");

            ball.SendMessage("resetBall", null, SendMessageOptions.RequireReceiver); // mengembalikan bola ke tengah
        }
        else if (player2.Score == maxScore)
        {
            // menampilkan teks player 1 win
            GUI.Label(new Rect((Screen.width / 2) + 150, (Screen.height / 2) - 15, 2000, 1000), "Player 2 Win");

            ball.SendMessage("resetBall", null, SendMessageOptions.RequireReceiver); // reset
        }

        if (isDebugWindowShown)
        {
            Color oldColor = GUI.backgroundColor;
            GUI.backgroundColor = Color.red; 

            float ballMass = ballRigidbody.mass;
            Vector2 ballVelocity = ballRigidbody.velocity;
            float ballSpeed = ballRigidbody.velocity.magnitude;
            Vector2 ballMomentum = ballMass * ballVelocity;
            float ballFriction = ballCollider.friction;

            float impulsePlayer1X = player1.LastContactPoint.normalImpulse;
            float impulsePlayer1Y = player1.LastContactPoint.tangentImpulse;
            float impulsePlayer2X = player2.LastContactPoint.normalImpulse;
            float impulsePlayer2Y = player2.LastContactPoint.tangentImpulse;

            string debugText =
                "Ball Mass = " + ballMass + "\n" +
                "Ball Velocity = " + ballVelocity + "\n" +
                "Ball Speed = " + ballSpeed + "\n" +
                "Ball Momentum = " + ballMomentum + "\n" +
                "Ball Friction = " + ballFriction + "\n" +
                "Last impulse from player 1 = (" + impulsePlayer1X + ", " + impulsePlayer1Y + ")\n" +
                "Last impulse from player 2 = (" + impulsePlayer2X + ", " + impulsePlayer2Y + ")\n";

            GUIStyle guiStyle = new GUIStyle(GUI.skin.textArea);
            guiStyle.alignment = TextAnchor.UpperCenter;
            GUI.TextArea(new Rect(Screen.width / 2 - 200, Screen.height - 200, 400, 110), debugText, guiStyle);

            GUI.backgroundColor = oldColor;
        }

        if (GUI.Button(new Rect(Screen.width / 2 - 60, Screen.height - 73, 120, 53), "TOGGLE\nDEBUG INFO"))
        {
            isDebugWindowShown = !isDebugWindowShown;
            trajectory.enabled = !trajectory.enabled;
        }
    }
}
